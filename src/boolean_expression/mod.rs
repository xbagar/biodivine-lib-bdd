//! Boolean expressions are simple structures that represent boolean formulas explicitly.
//!
//! They can be parsed from a string representation (using `TryFrom`) and used to create
//! complex `Bdd`s:
//!
//! ```rust
//! use biodivine_lib_bdd_generic::*;
//! let vars = BddVariableSet::new_anonymous(4);
//! let f: Bdd = vars.eval_expression_string("x_0 & !x_1 => (x_1 ^ x_3 <=> (x_0 | x_1))");
//! ```

/// **(internal)** Implements boolean expression evaluation for `BddVariableSet` and some utility methods.
mod _impl_boolean_expression;

/// **(internal)** Parsing functions for boolean expressions.
mod _impl_parser;

/// Recursive type for boolean expression tree.
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum BooleanExpression<N> {
    Const(bool),
    Variable(N),
    Not(Box<BooleanExpression<N>>),
    And(Box<BooleanExpression<N>>, Box<BooleanExpression<N>>),
    Or(Box<BooleanExpression<N>>, Box<BooleanExpression<N>>),
    Xor(Box<BooleanExpression<N>>, Box<BooleanExpression<N>>),
    Imp(Box<BooleanExpression<N>>, Box<BooleanExpression<N>>),
    Iff(Box<BooleanExpression<N>>, Box<BooleanExpression<N>>),
}
